# -*- coding: utf-8 -*-

#-----------------------------------------------------------------
# This file is a part of Pavages project.
# Name:         Pavages
# Copyright:    (C) 1998-2022 Pascal Peter
# License:      GNU General Public License version 3
# Website:      http://pascal.peter.free.fr
# Email:        pascal.peter at free.fr
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------

"""
DESCRIPTION :
    blablabla
"""


# importation des modules utiles :
import math

# importation des modules perso :
import utils, geom, pave

# PyQt5 :
from PyQt5 import QtCore, QtWidgets, QtGui



"""
****************************************************
Fonctions pour tracer les éléments du pavage.
****************************************************
"""

def drawFundamentalSet(
    main, painter, origine=None, withBrush=False, onlyPave=False, coeff=1):
    """
    Trace l'ensemble fondamental sur le painter choisi 
    (2 vues de l'écran ou SVG).
    Donc est appelé aussi bien pour tracer l'ensemble seul, 
    mais aussi le pavage complet et pour l'export en SVG.
        sommets donne la liste des sommets du pavé.
        origine décale le résultat (donc pour le pavage complet)
        withBrush indique s'il faut utiliser la brosse.
        onlyPave indique qu'on ne trace que le premier pavé.
    """
    width = main.monPave.lineWidth
    style = QtCore.Qt.PenStyle(main.monPave.lineStyle)
    cap = QtCore.Qt.PenCapStyle(main.monPave.lineCap)
    join = QtCore.Qt.PenJoinStyle(main.monPave.lineJoin)
    color = QtGui.QColor(
        main.monPave.lineColor[0], 
        main.monPave.lineColor[1], 
        main.monPave.lineColor[2], 
        main.monPave.lineColor[3])
    pen = QtGui.QPen(color, width, style, cap, join)
    painter.setPen(pen)
    sommets = [utils.internal2view(point) for point in main.monPave.sommets]
    polygon = QtGui.QPolygonF(sommets)
    # pour récupérer le boundingRect total :
    dimensions = [0, 0, 0, 0]
    if onlyPave:
        total = 1
    else:
        total = main.monPave.nbPaves
    for i in range(total):
        painter.save()
        color = main.monPave.colors[i]
        brush = QtGui.QBrush(
            QtGui.QColor(int(color[0]), int(color[1]), int(color[2]), int(color[3])))
        if withBrush:
            brush.setTexture(main.monPave.brush)
            brushTransform = QtGui.QTransform()
            boundingRect = polygon.boundingRect()
            bottomLeft = boundingRect.bottomLeft()
            brushTransform.translate(
                bottomLeft.x(), bottomLeft.y())
            coeff = 1 / coeff
            brushTransform.scale(coeff, coeff)
            brush.setTransform(brushTransform)
        painter.setBrush(brush)

        transformation = main.monPave.transformations[i]
        # la matrice des transformations :
        transform = QtGui.QTransform()
        # sans les translations :
        transform_linear = QtGui.QTransform()
        if transformation[0] == 'rotation':
            centre = utils.internal2view(transformation[1])
            a = geom.angleEnDegres(transformation[2])
            transform.translate(centre.x(), centre.y())
            transform.rotate(-a)
            transform.translate(-centre.x(), -centre.y())
            transform_linear.rotate(-a)

        elif transformation[0] == 'symetrieAxiale':
            O = geom.Point(X=0, Y=0)
            J = geom.Point(X=0, Y=1)
            Axe = geom.vecteur(transformation[1], transformation[2])
            vecteur = geom.projectionOrthogonale(
                O, transformation[1], transformation[2])
            vecteur = utils.internal2view(vecteur)
            a = geom.angleOriente(J, Axe)
            a = geom.angleEnDegres(a)
            transform.rotate(-a)
            transform.scale(-1, 1)
            transform.rotate(a)
            transform.translate(-2 * vecteur.x(), -2 * vecteur.y())
            transform_linear.rotate(-a)
            transform_linear.scale(-1, 1)
            transform_linear.rotate(a)

        elif transformation[0] == 'glissage':
            Axe = geom.vecteur(transformation[1], transformation[2])
            O = geom.Point(X=0, Y=0)
            J = geom.Point(X=0, Y=1)
            a = geom.angleOriente(J, Axe)
            vecteur = transformation[3]
            vecteur = geom.rotation(vecteur, O, -2 * a)
            vecteur = utils.internal2view(vecteur)
            a = geom.angleEnDegres(a)
            transform.rotate(-a)
            transform.scale(-1, 1)
            transform.rotate(a)
            transform.translate(-vecteur.x(), vecteur.y())
            transform_linear.rotate(-a)
            transform_linear.scale(-1, 1)
            transform_linear.rotate(a)

        elif transformation[0] == 'symetrieAxialeEtRotation':
            O = geom.Point(X=0, Y=0)
            J = geom.Point(X=0, Y=1)
            Axe = geom.vecteur(transformation[1], transformation[2])
            vecteur = geom.projectionOrthogonale(
                O, transformation[1], transformation[2])
            vecteur = utils.internal2view(vecteur)
            a = geom.angleOriente(J, Axe)
            a = geom.angleEnDegres(a)
            transform.rotate(-a)
            transform.scale(-1, 1)
            transform.rotate(a)
            transform.translate(-2 * vecteur.x(), -2 * vecteur.y())
            transform_linear.rotate(-a)
            transform_linear.scale(-1, 1)
            transform_linear.rotate(a)
            centre = transformation[3]
            centre = utils.internal2view(centre)
            centre = transform.map(centre)
            a = transformation[4]
            a = geom.angleEnDegres(a)
            transform.translate(centre.x(), centre.y())
            transform.rotate(-a)
            transform.translate(-centre.x(), -centre.y())
            transform_linear.rotate(-a)

        if origine != None:
            inverted = transform_linear.inverted()[0]
            P = utils.internal2view(origine) * inverted
            transform.translate(P.x(), P.y())

        painter.setWorldTransform(transform, combine=True)
        painter.drawPolygon(polygon)
        painter.restore()

        # on récupère le boundingRect du polygone transformé,
        # et on met à jour les dimensions :
        polygon2 = transform.map(polygon)
        rect2 = polygon2.boundingRect()
        if rect2.x() < dimensions[0]:
            dimensions[0] = rect2.x()
        if rect2.y() < dimensions[1]:
            dimensions[1] = rect2.y()
        if rect2.x() + rect2.width() > dimensions[2]:
            dimensions[2] = rect2.x() + rect2.width()
        if rect2.y() + rect2.height() > dimensions[3]:
            dimensions[3] = rect2.y() + rect2.height()
    return dimensions


