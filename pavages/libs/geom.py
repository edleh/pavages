# -*- coding: utf-8 -*-

#-----------------------------------------------------------------
# This file is a part of Pavages project.
# Name:         Pavages
# Copyright:    (C) 1998-2022 Pascal Peter
# License:      GNU General Public License version 3
# Website:      http://pascal.peter.free.fr
# Email:        pascal.peter at free.fr
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------

"""
DESCRIPTION :
    blablabla
"""


# importation des modules utiles :
import math



PI = math.pi
SQRT_3 = math.sqrt(3)



"""
****************************************************
    POINT
****************************************************
"""

CALCULE, LIE, LIBRE, FACULTATIF = 0, 1, 2, 3

class Point:
    """
    Un point et ses attributs utiles au programme
    """
    def __init__(self, X=0, Y=0, genre=CALCULE, arc=-1):
        # pour assurer des coordonnees reelles :
        self.X = X
        self.Y = Y
        self.genre = genre # Le genre de point
        self.arc = arc # Sur quel arc du pave est situe le point



"""
****************************************************
    FONCTIONS MATHÉMATIQUES UTILES
****************************************************
"""

def distance(A, B):
    return math.hypot((A.X - B.X), (A.Y - B.Y))

def milieu(A, B):
    return Point((A.X + B.X) / 2, (A.Y + B.Y) / 2)

def vecteur(A, B):
    return Point(B.X - A.X, B.Y - A.Y)

def produitScalaire(vecteur1, vecteur2):
    return vecteur1.X * vecteur2.X + vecteur1.Y * vecteur2.Y

def norme(V):
    return math.hypot(V.X, V.Y)

def normalise(V):
    """
    normalise le vecteur V
    """
    n = norme(V)
    if n > 0:
        newV = Point(V.X / n, V.Y / n)
    else:
        newV = V
    return newV

def cosinusScalaire(vecteur1, vecteur2):
    return (produitScalaire(vecteur1, vecteur2)) / (norme(vecteur1) * norme(vecteur2))

def angleOriente(vecteur1, vecteur2):
    # l'angle entre -pi et pi
    c = cosinusScalaire(vecteur1, vecteur2)
    s = vecteur1.X * vecteur2.Y - vecteur1.Y * vecteur2.X
    s = - s
    s = s / (norme(vecteur1) * norme(vecteur2))
    if c == 0:
        if s > 0:
            a =  PI / 2
        elif s < 0:
            a = - PI / 2
    elif (c > 0):
        a = math.atan(s / c)
    elif (c < 0):
        a = math.atan(s / c) + PI
    if a > PI:
        a = a - 2 * PI
    a = - a
    return a

def angleEnDegres(angleEnRadians):
    return angleEnRadians * 180 / PI



"""
****************************************************
    TRANSFORMATIONS D'UN POINT
****************************************************
"""

def rotation(P, centre, Angle):
    X = centre.X + math.cos(Angle) * (P.X - centre.X) - math.sin(Angle) * (P.Y - centre.Y)
    Y = centre.Y + math.sin(Angle) * (P.X - centre.X) + math.cos(Angle) * (P.Y - centre.Y)
    NewP = Point(X, Y)
    return NewP

def symetrieCentrale(P, centre):
    NewP = rotation(P, centre, PI)
    return NewP

def homothetie(P, centre, Rapport):
    X = centre.X + Rapport * (P.X - centre.X)
    Y = centre.Y + Rapport * (P.Y - centre.Y)
    NewP = Point(X, Y)
    return NewP

def similitude(P, centre, Angle, Rapport):
    NewP = rotation(P, centre, Angle)
    NewP = homothetie(NewP, centre, Rapport)
    return NewP

def translation(P, vecteur):
    NewP = Point(P.X + vecteur.X, P.Y + vecteur.Y)
    return NewP

def symetrieAxiale(P, A, B):
    i = 0
    if A.X == B.X:
        X = 2 * A.X - P.X
        Y = P.Y
        i = 1
    if (A.Y == B.Y) and (i == 0):
        X = P.X
        Y = 2 * A.Y - P.Y
        i = 1
    if i == 0:
        t = (B.Y - A.Y) / (B.X - A.X)
        X = (2 * (P.Y - A.Y + t * A.X) + (1 / t - t) * P.X) / (t + 1 / t)
        Y = P.Y - (X - P.X) / t
    NewP = Point(X, Y)
    return NewP

def glissage(P, A, B, vecteur):
    NewP = symetrieAxiale(P, A, B)
    NewP = translation(NewP, vecteur)
    return NewP

def projectionOrthogonale(P, A, B):
    """
    projection orthogonale sur la droite (AB)
    """
    if B.X == A.X:
      angle = PI / 2
    else:
      angle = math.atan((B.Y - A.Y) / (B.X - A.X))
    P2 = rotation(P, A, - angle)
    PH2 = Point(P2.X, A.Y)
    PH1 = rotation(PH2, A, angle)
    return PH1

def projection(P, A, B, vecteur):
    """
    projection sur la droite (AB) selon la direction donnée par le vecteur
    le systeme à résoudre est  ax+by=c dx+ey=f
    """
    a = -vecteur.Y
    b = vecteur.X
    c = a * P.X + b * P.Y
    d = A.Y - B.Y
    e = B.X - A.X
    f = d * B.X + e * B.Y
    det = a * e - b * d
    if det == 0:
        X = 0
        Y = 0
    else:
        X = (c * e - b * f) / det
        Y = (a * f - c * d) / det
    NewP = Point(X, Y)
    return NewP

def deformation(P, O, IEx, I, JEx, J):
    """
    """
    XM = projection(P, O, IEx, vecteur(O, JEx))
    YM = projection(P, O, JEx, vecteur(O, IEx))
    if IEx.Y == O.Y:
        a = (XM.X - O.X) / (IEx.X - O.X)
    else:
        a = (XM.Y - O.Y) / (IEx.Y - O.Y)
    if JEx.Y == O.Y:
        b = (YM.X - O.X) / (JEx.X - O.X)
    else:
        b = (YM.Y - O.Y) / (JEx.Y - O.Y)
    vecteur1 = vecteur(O, I)
    vecteur1.X = a * vecteur1.X
    vecteur1.Y = a * vecteur1.Y
    XM = translation(O, vecteur1)
    vecteur2 = vecteur(O, J)
    vecteur2.X = b * vecteur2.X
    vecteur2.Y = b * vecteur2.Y
    YM = translation(O, vecteur2)
    vecteur1 = vecteur(O, YM)
    NewP = translation(XM, vecteur1)
    return NewP

def distanceSegment(P, A, B):
    """
    distance entre le point P et le segment [AB]
    """
    result = distance(P, A)
    distance_B = distance(P, B)
    if distance_B < result:
        result = distance_B
    H = projectionOrthogonale(P, A, B)
    if distance(H, A) + distance(H, B) == distance(A, B):
        distance_H = distance(P, H)
        if distance_H < result:
            result = distance_H
    return result

def intersectionSegments(A, B, C, D):
    """
    détermine si les segments [AB] et [CD] se coupent
    """
    denominateur = (B.X - A.X) * (D.Y - C.Y) - (B.Y - A.Y) * (D.X - C.X)
    if denominateur == 0:
        return False
    r_numerateur = (A.Y - C.Y) * (D.X - C.X) - (A.X - C.X) * (D.Y - C.Y)
    r = r_numerateur / denominateur
    if (r <= 0 or r >= 1):
        return False
    s_numerateur = (A.Y - C.Y) * (B.X - A.X) - (A.X - C.X) * (B.Y - A.Y)
    s = s_numerateur / denominateur
    if (s <= 0 or s >= 1):
        return False
    return True


