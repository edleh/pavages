# -*- coding: utf-8 -*-

#-----------------------------------------------------------------
# This file is a part of Pavages project.
# Name:         Pavages
# Copyright:    (C) 1998-2022 Pascal Peter
# License:      GNU General Public License version 3
# Website:      http://pascal.peter.free.fr
# Email:        pascal.peter at free.fr
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------

"""
DESCRIPTION :
    Ce module contient des fonctions utiles au programme.
"""


# importation des modules perso :
import utils

# PyQt5 :
from PyQt5 import QtCore, QtWidgets, QtGui



"""
****************************************************
    POUR L'AFFICHAGE DES TEXTES
****************************************************
"""

def myPrint(*args):
    if len(args) > 1:
        print(args)
    else:
        arg = args[0]
        try:
            print(arg)
        except:
            try:
                print(u(arg))
            except:
                try:
                    print(s(arg))
                except:
                    print('PB in myPrint')

def u(text):
    # retourne une version unicode de text
    try:
        if isinstance(text, str):
            return text
        else:
            return str(text)
    except:
        myPrint('ERROR utils.u', type(text), text)
        return text

def s(text):
    # retourne une version str de text
    if isinstance(text, str):
        return text
    else:
        try:
            return str(text)
        except:
            myPrint('ERROR utils.s', type(text), text)
            return text



"""
****************************************************
    MESSAGES, BOUTONS, ...
****************************************************
"""

NOWAITCURSOR = False
def changeNoWaitCursor(newNoWaitCursor):
    global NOWAITCURSOR
    NOWAITCURSOR = newNoWaitCursor

def doWaitCursor():
    if not(NOWAITCURSOR):
        QtWidgets.QApplication.setOverrideCursor(QtCore.Qt.WaitCursor)

def restoreCursor():
    if not(NOWAITCURSOR):
        QtWidgets.QApplication.restoreOverrideCursor()



"""
****************************************************
    DIVERS
****************************************************
"""

def doLocale(locale, beginFileName, endFileName, defaultFileName=''):
    """
    Teste l'existence d'un fichier localisé.
    Par exemple, insère _fr_FR ou _fr entre beginFileName et endFileName.
    Renvoie le fichier par défaut sinon.
    """
    # on teste d'abord avec locale (par exemple fr_FR) :
    localeFileName = u('{0}_{1}{2}').format(beginFileName, locale, endFileName)
    if QtCore.QFileInfo(localeFileName).exists():
        return localeFileName
    # ensuite avec lang (par exemple fr) :
    lang = locale.split('_')[0]
    localeFileName = u('{0}_{1}{2}').format(beginFileName, lang, endFileName)
    if QtCore.QFileInfo(localeFileName).exists():
        return localeFileName
    # si defaultFileName est spécifié :
    if defaultFileName != '':
        return u(defaultFileName)
    # sinon on renvoie le fichier de départ :
    localeFileName = u('{0}{1}').format(beginFileName, endFileName)
    return localeFileName

def addSlash(aDir):
    """
    pour ajouter un / à la fin d'un nom de dossier si besoin
    aDir = utils_functions.addSlash(aDir)
    """
    if aDir[-1] != '/':
        aDir = aDir + '/'
    return aDir

def removeSlash(aDir):
    """
    pour supprimer l'éventuel / à la fin d'un nom de dossier
    aDir = utils_functions.removeSlash(aDir)
    """
    if len(aDir) > 0:
        if aDir[-1] == '/':
            aDir = aDir[:-1]
    return aDir

def verifyLibs_fileName(fileName):
    """
    selon la version de PyQt, QFileDialog renvoie soit 
    * un string : fileName
    * un tuple : (fileName, extension)
    """
    if isinstance(fileName, tuple):
        fileName = fileName[0]
    return fileName


