# -*- coding: utf-8 -*-

#-----------------------------------------------------------------
# This file is a part of Pavages project.
# Name:         Pavages
# Copyright:    (C) 1998-2022 Pascal Peter
# License:      GNU General Public License version 3
# Website:      http://pascal.peter.free.fr
# Email:        pascal.peter at free.fr
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------

"""
DESCRIPTION :
    Affichage de l'ensemble fondamental 
    avec son pavé de base, et du pavage, 
    chacun dans sa fenêtre.
"""


# importation des modules perso :
import utils, utils_functions, geom, pave, pavage

# PyQt5 :
from PyQt5 import QtCore, QtWidgets, QtGui



NORMAL_SIZE = 5
SELECTED_SIZE = 7


class Node(QtWidgets.QGraphicsItem):
    """
    Les Points déplaçables affichés à l'écran.
    Repris de l'exemple "elasticnodes" de PyQt.
    Le Node doit pouvoir avertir le GraphicsViewPave
    de tout changement le concernant (déplacement, etc...)
    """
    def __init__(self, graphicsViewPave, point):
        super(Node, self).__init__()
        self.graphicsViewPave = graphicsViewPave
        self.point = point
        self.main = graphicsViewPave.main

        # les flags et autres trucs qui vont bien 
        # (pas forcément tous indispensables ici) :
        self.setFlag(QtWidgets.QGraphicsItem.ItemIsSelectable)
        self.setFlag(QtWidgets.QGraphicsItem.ItemIsMovable)
        self.setFlag(QtWidgets.QGraphicsItem.ItemSendsGeometryChanges)
        # décommenter la ligne suivante si on veut que les points 
        # gardent la même taille lors d'un zoom :
        #self.setFlags(QtWidgets.QGraphicsItem.ItemIgnoresTransformations)
        self.setCacheMode(QtWidgets.QGraphicsItem.DeviceCoordinateCache)
        self.setAcceptHoverEvents(True)
        self.setZValue(2)

        self.lastPos = self.pos()
        self.lastJ = (0, 1)
        self.isSelected = False
        self.size = NORMAL_SIZE
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.timerFinished)

    def advance(self):
        if self.isSelected:
            if self.point == self.main.monPave.I:
                self.graphicsViewPave.applyMoveI(
                    self.lastPos, self.pos())
            elif self.point == self.main.monPave.J:
                self.main.monPave.calculer()
                self.graphicsViewPave.applyMoveJ(
                    self.lastJ, (self.point.X, self.point.Y))
                self.lastJ = (self.point.X, self.point.Y)
            else:
                croisements = self.main.monPave.testCroisements()
                if croisements:
                    text = QtWidgets.QApplication.translate(
                        'main', 'The tile is crossed!')
                else:
                    text = ''
                text = utils_functions.u(
                    '<p style="font-size:14px;font-weight:bold;color:red">{0}</p>').format(text)
                self.graphicsViewPave.main.messageLabel.setText(text)
            if self.point.genre == geom.LIE:
                self.graphicsViewPave.replaceLies()
            (self.point.X, self.point.Y) = utils.view2internal(self.pos())
            self.graphicsViewPave.fundamentalSet.update()
        self.lastPos = self.pos()

    def boundingRect(self):
        adjust = 1
        a = - self.size - adjust
        b = 2.1 * self.size + adjust
        return QtCore.QRectF(a, a, b, b)

    def shape(self):
        path = QtGui.QPainterPath()
        a = -self.size
        b = 2 * self.size
        path.addEllipse(a, a, b, b)
        return path

    def paint(self, painter, option, widget):
        painter.setPen(QtCore.Qt.NoPen)
        painter.setBrush(QtCore.Qt.darkGray)
        a = int(-0.7 * self.size)
        b = int(2 * self.size)
        painter.drawEllipse(a, a, b, b)
        if self.point.genre == geom.FACULTATIF:
            color = QtCore.Qt.blue
            darkcolor = QtCore.Qt.darkBlue
        else:
            color = QtCore.Qt.yellow
            darkcolor = QtCore.Qt.darkYellow
        a = -0.3 * self.size
        gradient = QtGui.QRadialGradient(a, a, self.size)
        if option.state and QtWidgets.QStyle.State_Sunken:
            a = 0.3 * self.size
            gradient.setCenter(a, a)
            gradient.setFocalPoint(a, a)
            gradient.setColorAt(1, QtGui.QColor(color).lighter(120))
            gradient.setColorAt(0, QtGui.QColor(darkcolor).lighter(120))
        else:
            gradient.setColorAt(0, color)
            gradient.setColorAt(1, darkcolor)
        painter.setBrush(QtGui.QBrush(gradient))
        painter.setPen(QtGui.QPen(QtCore.Qt.black, 0))
        a = int(2 * self.size)
        painter.drawEllipse(-self.size, -self.size, a, a)

    def itemChange(self, change, value):
        """
        explications
        """
        if change == QtWidgets.QGraphicsItem.ItemPositionChange:
            self.graphicsViewPave.itemMoved()
        return QtWidgets.QGraphicsItem.itemChange(self, change, value)

    def mousePressEvent(self, event):
        self.graphicsViewPave.scene().clearSelection()
        self.isSelected = True
        self.size = SELECTED_SIZE
        self.update()
        QtWidgets.QGraphicsItem.mousePressEvent(self, event)

    def mouseReleaseEvent(self, event):
        if self.isSelected:
            if self.point.genre == geom.LIE:
                self.main.monPave.calculer()
                self.setPos(utils.internal2view(self.point))
            (self.point.X, self.point.Y) = utils.view2internal(self.pos())
            self.graphicsViewPave.fundamentalSet.update()
            self.lastPos = self.pos()
            self.size = NORMAL_SIZE
            self.update()
            self.graphicsViewPave.fautMettreAJour(paveModifie=True)
            QtWidgets.QGraphicsItem.mouseReleaseEvent(self, event)
            self.timer.start(50)

    def timerFinished(self):
        self.timer.stop()
        self.isSelected = False


class FundamentalSet(QtWidgets.QGraphicsItem):
    """
    Le GraphicsItem qui affiche l'ensemble fondamental.
    Mais aussi les vecteurs de la base, et les Nodes.
    """
    def __init__(self, graphicsViewPave):
        super(FundamentalSet, self).__init__()
        self.graphicsViewPave = graphicsViewPave
        self.main = self.graphicsViewPave.main

        self.setZValue(1)
        self.pen = QtGui.QPen()
        self.brush = QtGui.QBrush()
        self.dimensions = [0, 0, 0, 0]

    def boundingRect(self):
        adjust = utils.SCENE_SIZE / self.graphicsViewPave.actualScale
        bound = QtCore.QRectF(-adjust, -adjust, 2 * adjust, 2 * adjust)
        return bound

    def paint(self, painter, option, widget):
        self.doPaint(painter)

    def doPaint(self, painter, repere=True, onlyPave=False):
        """
        explications
        """
        self.main.monPave.calculer()
        # L'ensemble fondamental :
        self.dimensions = pavage.drawFundamentalSet(
            self.main, 
            painter, 
            withBrush=self.graphicsViewPave.withBrush, 
            onlyPave=onlyPave)
        # Le repère :
        if repere:
            self.pen.setWidth(10)
            painter.setPen(self.pen)
            painter.setPen(QtGui.QColor(127, 255, 127, 255))
            painter.setBrush(QtGui.QColor(127, 255, 127, 255))
            QO = utils.internal2view(self.main.monPave.O)
            QI = utils.internal2view(self.main.monPave.I)
            QJ = utils.internal2view(self.main.monPave.J)
            painter.drawLine(int(QO.x()), int(QO.y()), int(QI.x()), int(QI.y()))
            painter.drawLine(int(QO.x()), int(QO.y()), int(QJ.x()), int(QJ.y()))
            painter.drawPath(self.drawFleche(self.main.monPave.O, self.main.monPave.I, 20))
            painter.drawPath(self.drawFleche(self.main.monPave.O, self.main.monPave.J, 20))

    def drawFleche(self, A, B, Taille):
        """
        explications
        """
        path = QtGui.QPainterPath()
        QB = utils.internal2view(B)
        path.moveTo(QB.x(), QB.y())
        V = geom.vecteur(A, B)
        V = geom.normalise(V)
        V.X, V.Y = Taille * V.X, Taille * V.Y
        C = geom.Point(0, 0)
        P = geom.rotation(V, C, 4 * geom.PI / 3)
        path.moveTo(QB.x() + P.X, QB.y() - P.Y)
        P = geom.rotation(V, C, 0)
        path.lineTo(QB.x() + P.X, QB.y() - P.Y)
        P = geom.rotation(V, C, 2 * geom.PI / 3)
        path.lineTo(QB.x() + P.X, QB.y() - P.Y)
        #path.closeSubpath()
        return path


class GraphicsItemPavage(QtWidgets.QGraphicsItem):
    """
    Le GraphicsItem qui affiche le pavage.
    """
    def __init__(self, graphicsViewPavage):
        super(GraphicsItemPavage, self).__init__()
        self.graphicsViewPavage = graphicsViewPavage
        self.main = graphicsViewPavage.main

        self.setZValue(1)
        self.nbX, self.nbY = 5, 2
        self.dimensions = [0, 0, 0, 0]

    def advance(self):
        if self.newPos == self.pos():
            return False
        self.setPos(self.newPos)
        return True

    def boundingRect(self):
        return QtCore.QRectF(
            utils.SCENE_BEGIN - 10, 
            utils.SCENE_BEGIN - 10, 
            utils.SCENE_SIZE + 23, 
            utils.SCENE_SIZE + 23)

    def paint(self, painter, option, widget):
        self.doPaint(painter)

    def doPaint(self, painter):
        """
        explications
        """
        painter.setBrush(QtCore.Qt.darkGray)

        width = self.main.monPave.lineWidth
        style = QtCore.Qt.PenStyle(self.main.monPave.lineStyle)
        cap = QtCore.Qt.PenCapStyle(self.main.monPave.lineCap)
        join = QtCore.Qt.PenJoinStyle(self.main.monPave.lineJoin)
        color = QtGui.QColor(
            self.main.monPave.lineColor[0], 
            self.main.monPave.lineColor[1], 
            self.main.monPave.lineColor[2], 
            self.main.monPave.lineColor[3])
        painter.setPen(QtGui.QPen(color, width, style, cap, join))

        # L'ensemble fordamental :
        nbX = self.nbX // 2
        if 2 * nbX == self.nbX:
            rX = 0
        else:
            rX = 1
        nbY = self.nbY // 2
        if 2 * nbY == self.nbY:
            rY = 0
        else:
            rY = 1
        self.dimensions = [0, 0, 0, 0]
        for n in range(-nbX, nbX + rX):
            for m in range(-nbY, nbY + rY):
                xx = n * self.main.monPave.I.X + m * self.main.monPave.J.X
                yy = n * self.main.monPave.I.Y + m * self.main.monPave.J.Y
                origine = geom.Point(xx, yy)
                dimensions = pavage.drawFundamentalSet(
                    self.main, 
                    painter, 
                    origine, 
                    withBrush=self.graphicsViewPavage.withBrush)
                if dimensions[0] < self.dimensions[0]:
                    self.dimensions[0] = dimensions[0]
                if dimensions[1] < self.dimensions[1]:
                    self.dimensions[1] = dimensions[1]
                if dimensions[2] > self.dimensions[2]:
                    self.dimensions[2] = dimensions[2]
                if dimensions[3] > self.dimensions[3]:
                    self.dimensions[3] = dimensions[3]


class GraphicsViewPave(QtWidgets.QGraphicsView):
    """
    Le GraphicsView qui encapsule l'affichage de l'ensemble fondamental.
    Gère aussi les actions clic-droit et menu pop-up.
    """
    def __init__(self, parent=None):
        super(GraphicsViewPave, self).__init__(parent)
        self.main = parent

        self.timerId = 0
        self.withBrush = False

        scene = QtWidgets.QGraphicsScene(self)
        scene.setItemIndexMethod(QtWidgets.QGraphicsScene.NoIndex)
        self.setScene(scene)
        self.setCacheMode(QtWidgets.QGraphicsView.CacheBackground)
        self.setRenderHint(QtGui.QPainter.Antialiasing)
        self.setTransformationAnchor(QtWidgets.QGraphicsView.AnchorUnderMouse)
        self.setResizeAnchor(QtWidgets.QGraphicsView.AnchorViewCenter)

        self.fundamentalSet = FundamentalSet(self)
        scene.addItem(self.fundamentalSet)
        self.fundamentalSet.setPos(0, 0)

        self.createnodes()
        self.createActions()
        self.selectedNode = None

        self.actualScale = 1
        self.scale(self.actualScale, self.actualScale)
        self.translateUi()

        self.setAcceptDrops(True)

    def createnodes(self):
        """
        suppression des anciens nodes et creation des nouveaux
        """
        nodes = [item for item in self.scene().items() if isinstance(item, Node)]
        for node in nodes:
            self.scene().removeItem(node)
        # Les points libres :
        for point in self.main.monPave.controlPoints:
            if point.genre > 0:
                QP = utils.internal2view(point)
                newNode = Node(self, point)
                self.scene().addItem(newNode)
                newNode.setPos(QP.x(), QP.y())

    def applyMoveI(self, lastPos, newPos):
        """
        Lorsque le point I est déplacé, on déplace tout le pavé avec.
        C'est une similitude qui est à appliquer aux autres points.
        """
        angle = 0
        rapport = 1
        (X, Y) = utils.view2internal(lastPos)
        lastOI = geom.vecteur(self.main.monPave.O, geom.Point(X, Y))
        (X, Y) = utils.view2internal(newPos)
        newOI = geom.vecteur(self.main.monPave.O, geom.Point(X, Y))
        try:
            angle = geom.angleOriente(lastOI, newOI)
            lastNorme = geom.norme(lastOI)
            newNorme = geom.norme(newOI)
            rapport = newNorme / lastNorme
        except:
            pass
        angle = geom.angleEnDegres(angle)
        transform = QtGui.QTransform().rotate(-angle).scale(rapport, rapport)

        nodes = [item for item in self.scene().items() if isinstance(item, Node) \
            and not(item.point in (self.main.monPave.O, self.main.monPave.I))]
        for node in nodes:
            QP = utils.internal2view(node.point)
            newQP = transform.map(QP)
            node.setPos(newQP.x(), newQP.y())
            (node.point.X, node.point.Y) = utils.view2internal(node.pos())

    def applyMoveJ(self, lastJ, newJ):
        """
        Lorsque le point J est déplacé, on déforme les sommets du pavé.
        Seuls O et I restent fixes.
        On utilise les coordonnées internes car le node affiché peut être en dehors
        de la vraie position de J (en cas de contrainte sur J).
        """
        O = self.main.monPave.O
        I = self.main.monPave.I
        JEx = geom.Point(lastJ[0], lastJ[1])
        J = geom.Point(newJ[0], newJ[1])

        nodes = [item for item in self.scene().items() if isinstance(item, Node) \
            and not(item.point in (O, I, self.main.monPave.J))]
        for node in nodes:
            newP = geom.deformation(node.point, O, I, I, JEx, J)
            QP = utils.internal2view(newP)
            node.setPos(QP.x(), QP.y())
            (node.point.X, node.point.Y) = utils.view2internal(node.pos())

    def replaceLies(self):
        """
        Replace les nodes de tous les points liés.
        À faire après avoir déplacé un point lié.
        """
        nodes = [item for item in self.scene().items() if isinstance(item, Node) \
            and (item.point.genre == geom.LIE) and not(item.isSelected)]
        for node in nodes:
            QP = utils.internal2view(node.point)
            node.setPos(QP.x(), QP.y())

    def createActions(self):
        self.actionNewPoint = QtWidgets.QAction(self, 
            icon=QtGui.QIcon('images/point-add.png'),
            triggered=self.newPoint)
        self.actionDeletePoint = QtWidgets.QAction(self, 
            icon=QtGui.QIcon('images/point-delete.png'),
            triggered=self.deletePoint)
        self.createPointActions = []
        for i in range(4):
            newAct = QtWidgets.QAction(self, 
                icon=QtGui.QIcon('images/point-add.png'),
                triggered=self.createPoint)
            newAct.setData(i) #important
            self.createPointActions.append(newAct)
        self.paveColorActions = []
        for i in range(12):
            newAct = QtWidgets.QAction(self, 
                icon=QtGui.QIcon('images/aaa.png'),
                triggered=self.pavecolor)
            newAct.setData(i) #important
            self.paveColorActions.append(newAct)
        self.actionLineStyle = QtWidgets.QAction(self, 
            icon=QtGui.QIcon('images/line-style.png'),
            triggered=self.main.setPenProperties)
        self.actionSelectBrush = QtWidgets.QAction(self, 
            icon=QtGui.QIcon('images/brush-select.png'),
            triggered=self.main.selectBrush)
        self.actionPavageConfig = QtWidgets.QAction(self, 
            icon=QtGui.QIcon('images/configure.png'),
            triggered=self.main.setPavageConfig)

    def translateUi(self):
        self.actionNewPoint.setText(
            QtWidgets.QApplication.translate('main', 'Duplicate this Point'))
        self.actionNewPoint.setStatusTip(
            QtWidgets.QApplication.translate(
                'main', 'A new Point will be create with the same position'))
        self.actionDeletePoint.setText(
            QtWidgets.QApplication.translate('main', 'Delete this Point'))
        self.actionDeletePoint.setStatusTip(
            QtWidgets.QApplication.translate('main', 'Delete this Point'))
        self.actionLineStyle.setText(
            QtWidgets.QApplication.translate('main', 'Line Style'))
        self.actionLineStyle.setStatusTip(
            QtWidgets.QApplication.translate('main', 'Set the Line Style'))
        self.actionSelectBrush.setText(
            QtWidgets.QApplication.translate('main', 'Select Brush'))
        self.actionSelectBrush.setStatusTip(
            QtWidgets.QApplication.translate('main', 'Select the Brush'))
        self.actionPavageConfig.setText(
            QtWidgets.QApplication.translate('main', 'Tiling Config'))
        self.actionPavageConfig.setStatusTip(
            QtWidgets.QApplication.translate(
                'main', 'Set the Tiling Config'))
        self.updateEditMenu()

    def newPoint(self):
        self.main.monPave.ajoutePoint(self.selectedNode.point)
        self.createnodes()
        self.main.updateTitle(mustSave=True)

    def deletePoint(self):
        self.main.monPave.supprimePoint(self.selectedNode.point)
        self.createnodes()
        self.update()
        self.fautMettreAJour(paveModifie=True)

    def createPoint(self):
        action = self.sender()
        i = action.data()
        self.main.monPave.creePoint(i)
        self.main.monPave.calculer()
        self.createnodes()
        self.main.updateTitle(mustSave=True)

    def pavecolor(self):
        action = self.sender()
        index = action.data()
        color = self.main.monPave.colors[index]
        color = QtGui.QColor(int(color[0]), int(color[1]), int(color[2]), int(color[3]))
        color = QtWidgets.QColorDialog.getColor(
            color, self, '', QtWidgets.QColorDialog.ShowAlphaChannel)
        if color.isValid():
            self.main.monPave.colors[index][0] = color.red()
            self.main.monPave.colors[index][1] = color.green()
            self.main.monPave.colors[index][2] = color.blue()
            self.main.monPave.colors[index][3] = color.alpha()
            self.fautMettreAJour(paveModifie=True)

    def updateEditMenu(self, aNodeIsSelected=False):
        self.main.editMenu.clear()
        # Si un point est sélectionné :
        if aNodeIsSelected:
            # Dupliquer :
            self.main.editMenu.addAction(self.actionNewPoint)
            # Supprimer :
            self.main.editMenu.addAction(self.actionDeletePoint)
        # Créer un nouveau point sur un arc :
        self.main.editMenu.addSeparator()
        for i in range(self.main.monPave.nbArcs):
            text = QtWidgets.QApplication.translate(
                'main', 'Create a Point on arc {0}')
            text = utils_functions.u(text).format(i + 1)
            self.main.editMenu.addAction(self.createPointActions[i])
            self.createPointActions[i].setText(text)
            self.createPointActions[i].setStatusTip(
                QtWidgets.QApplication.translate(
                    'main', 'Create a new Point'))
        # Sous-menu Couleurs des pavés :
        self.main.editMenu.addSeparator()
        menucolors = self.main.editMenu.addMenu(
            QtWidgets.QApplication.translate('main', '&Colors'))
        menucolors.setIcon(QtGui.QIcon('images/colorize.png'))
        for i in range(self.main.monPave.nbPaves):
            text = QtWidgets.QApplication.translate(
                'main', 'Color Tile {0}')
            text = utils_functions.u(text).format(i + 1)
            menucolors.addAction(self.paveColorActions[i])
            self.paveColorActions[i].setText(text)
            self.paveColorActions[i].setStatusTip(
                QtWidgets.QApplication.translate(
                    'main', 'Change the Tile Color'))
        self.main.editMenu.addAction(self.actionLineStyle)
        self.main.editMenu.addSeparator()
        self.main.editMenu.addAction(self.main.actionBrush)
        self.main.editMenu.addAction(self.actionSelectBrush)
        self.main.editMenu.addSeparator()
        self.main.editMenu.addAction(self.actionPavageConfig)

    def contextMenuEvent(self, event):
        nodes = [item for item in self.scene().items() if isinstance(item, Node)]
        aNodeIsSelected = False
        for node in nodes:
            if node.isSelected and (node.point.genre == geom.FACULTATIF):
                self.selectedNode = node
                aNodeIsSelected = True
        self.updateEditMenu(aNodeIsSelected)
        self.main.editMenu.exec_(event.globalPos())
        for node in nodes:
            if node.isSelected:
                node.setSelected(False)
                node.size = NORMAL_SIZE
                node.update()
        self.selectedNode = None
        self.updateEditMenu()

    def itemMoved(self):
        if not self.timerId:
            self.timerId = self.startTimer(1000 // 25)

    def timerEvent(self, event):
        nodes = [item for item in self.scene().items() if isinstance(item, Node)]
        for node in nodes:
            node.newPos = node.pos()
        itemsMoved = False
        for node in nodes:
            if node.advance():
                itemsMoved = True
        if not itemsMoved:
            self.killTimer(self.timerId)
            self.timerId = 0

    def wheelEvent(self, event):
        if utils.PYQT == 'PYQT5':
            angle = event.angleDelta().y()
        else:
            angle = event.delta()
        if angle > 0:
            scale = 1.1
        else:
            scale = 10 / 11
        self.scale(scale, scale)
        self.actualScale = self.actualScale * scale

    def fautMettreAJour(self, paveModifie=False, paveCalculer=False):
        if paveCalculer:
            self.main.updateViews(
                paveModifie=paveModifie, 
                affichagePave=False, 
                affichagePavage=False, 
                paveCalculer=True)
        else:
            self.main.updateViews(paveModifie)

    def dragEnterEvent(self, event):
        """
        on accepte le drag seulement si c'est un fichier *.pav
        """
        accept = False
        if event.mimeData().hasUrls:
            try:
                fileName = event.mimeData().urls()[0].toLocalFile()
                extension = QtCore.QFileInfo(
                    fileName).completeSuffix().lower()
                if extension == 'pav':
                    accept = True
            except:
                pass
        if accept:
            event.accept()
        else:
            event.ignore()

    def dragMoveEvent(self, event):
        if event.mimeData().hasUrls:
            event.setDropAction(QtCore.Qt.CopyAction)
            event.accept()
        else:
            event.ignore()

    def dropEvent(self, event):
        """
        on demande l'ouverture du fichier
        """
        if event.mimeData().hasUrls:
            event.setDropAction(QtCore.Qt.CopyAction)
            event.accept()
            try:
                fileName = utils_functions.u(
                    event.mimeData().urls()[0].toLocalFile())
                self.main.fileOpen(fileName)
            except:
                pass
        else:
            event.ignore()


class GraphicsViewPavage(QtWidgets.QGraphicsView):
    """
    Le GraphicsView qui affiche le pavage.
    """
    def __init__(self, parent=None):
        super(GraphicsViewPavage, self).__init__(parent)
        self.main = parent

        self.withBrush = False
        scene = QtWidgets.QGraphicsScene(self)
        scene.setItemIndexMethod(QtWidgets.QGraphicsScene.NoIndex)
        self.setScene(scene)
        self.setCacheMode(QtWidgets.QGraphicsView.CacheBackground)
        self.setRenderHint(QtGui.QPainter.Antialiasing)
        self.setTransformationAnchor(QtWidgets.QGraphicsView.AnchorUnderMouse)
        self.setResizeAnchor(QtWidgets.QGraphicsView.AnchorViewCenter)

        self.GraphicsItemPavage = GraphicsItemPavage(self)
        scene.addItem(self.GraphicsItemPavage)
        self.GraphicsItemPavage.setPos(0, 0)

        self.scale(0.3, 0.3)

    def wheelEvent(self, event):
        if utils.PYQT == 'PYQT5':
            angle = event.angleDelta().y()
        else:
            angle = event.delta()
        if angle > 0:
            scale = 1.1
        else:
            scale = 10 / 11
        self.scale(scale, scale)


