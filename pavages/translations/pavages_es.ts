<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="2.0">
<context>
    <name>main</name>
    <message>
        <location filename="../libs/utils_about.py" line="103"/>
        <source>&amp;Close</source>
        <translation>&amp;Cerrar</translation>
    </message>
    <message>
        <location filename="../libs/utils_about.py" line="95"/>
        <source>About</source>
        <translation>Acerca de</translation>
    </message>
    <message>
        <location filename="../libs/utils_about.py" line="98"/>
        <source>License</source>
        <translation>Licencia</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="514"/>
        <source>About this Application</source>
        <translation>Acerca de esta aplicación</translation>
    </message>
    <message>
        <location filename="../libs/views.py" line="472"/>
        <source>Duplicate this Point</source>
        <translation>Duplicar este punto</translation>
    </message>
    <message>
        <location filename="../libs/views.py" line="479"/>
        <source>Delete this Point</source>
        <translation>Eliminar este punto</translation>
    </message>
    <message>
        <location filename="../libs/views.py" line="545"/>
        <source>Create a new Point</source>
        <translation>Crear nuevo punto</translation>
    </message>
    <message>
        <location filename="../libs/views.py" line="559"/>
        <source>Change the Tile Color</source>
        <translation>Cambiar el color de la tesela</translation>
    </message>
    <message>
        <location filename="../libs/views.py" line="481"/>
        <source>Line Style</source>
        <translation>Estilo de línea</translation>
    </message>
    <message>
        <location filename="../libs/views.py" line="483"/>
        <source>Set the Line Style</source>
        <translation>Establecer el estilo de línea</translation>
    </message>
    <message>
        <location filename="../libs/views.py" line="485"/>
        <source>Select Brush</source>
        <translation>Seleccionar pincel</translation>
    </message>
    <message>
        <location filename="../libs/views.py" line="487"/>
        <source>Select the Brush</source>
        <translation>Seleccionar el pincel</translation>
    </message>
    <message>
        <location filename="../libs/views.py" line="540"/>
        <source>Create a Point on arc {0}</source>
        <translation>Crear un punto en el arco {0}</translation>
    </message>
    <message>
        <location filename="../libs/views.py" line="550"/>
        <source>&amp;Colors</source>
        <translation>&amp;Colores</translation>
    </message>
    <message>
        <location filename="../libs/views.py" line="554"/>
        <source>Color Tile {0}</source>
        <translation>Tesela de color {0}</translation>
    </message>
    <message>
        <location filename="../libs/views.py" line="474"/>
        <source>A new Point will be create with the same position</source>
        <translation>Se creará un punto nuevo en la misma posición</translation>
    </message>
    <message>
        <location filename="../libs/views.py" line="489"/>
        <source>Tiling Config</source>
        <translation>Configuración del teselado</translation>
    </message>
    <message>
        <location filename="../libs/views.py" line="491"/>
        <source>Set the Tiling Config</source>
        <translation>Establecer la configuración del teselado</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="426"/>
        <source>&amp;File</source>
        <translation>&amp;Archivo</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="506"/>
        <source>&amp;Help</source>
        <translation>Ay&amp;uda</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="423"/>
        <source>toolBarFile</source>
        <translation>toolBarFile</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="428"/>
        <source>&amp;Open</source>
        <translation>A&amp;brir</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="430"/>
        <source>Open a File</source>
        <translation>Abrir un archivo</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="432"/>
        <source>&amp;Save</source>
        <translation>&amp;Guardar</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="434"/>
        <source>Save the File</source>
        <translation>Guardar el archivo</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="436"/>
        <source>Save &amp;as</source>
        <translation>Guardar &amp;como</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="451"/>
        <source>&amp;Quit</source>
        <translation>&amp;Salir</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="453"/>
        <source>Quit Application</source>
        <translation>Salir de la aplicación</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="512"/>
        <source>&amp;About</source>
        <translation>&amp;Acerca de</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="458"/>
        <source>&amp;Brush</source>
        <translation>&amp;Pincel</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="503"/>
        <source>Export the Tiling as SVG File</source>
        <translation>Expotar el teselado como un archivo SVG</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="481"/>
        <source>Export the Tile as PNG File</source>
        <translation>Exportar la tesela como archivo PNG</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="438"/>
        <source>Save the File with a different name</source>
        <translation>Guardar el archivo con un nombre diferente</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="441"/>
        <source>&amp;Language</source>
        <translation>&amp;Idioma</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="443"/>
        <source>Choose Language</source>
        <translation>Elija idioma</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="90"/>
        <source>LanguageName</source>
        <translation>España</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="829"/>
        <source>Open</source>
        <translation>Abrir</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="860"/>
        <source>Pavage Files (*.pav)</source>
        <translation>Archivos Pavage (*.pav)</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="860"/>
        <source>Save as</source>
        <translation>Guardar como</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="471"/>
        <source>SVG Files (*.svg)</source>
        <translation>Archivos SVG (*.svg)</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="761"/>
        <source>PNG Files (*.png)</source>
        <translation>Archivos PNG (*.png)</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="791"/>
        <source>File must be saved ?</source>
        <translation>¿Guardar archivo?</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="793"/>
        <source>Save ?</source>
        <translation>¿Guardar?</translation>
    </message>
    <message>
        <location filename="../libs/utils_exports.py" line="376"/>
        <source>Cannot write file {0}:
{1}.</source>
        <translation>No se puede escribir el archivo {0}:
{1}.</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="523"/>
        <source>p1</source>
        <translation>p1</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="523"/>
        <source>asymmetric parallelogramic</source>
        <translation>paralelogramo asimétrico</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="525"/>
        <source>p2</source>
        <translation>p2</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="525"/>
        <source>symmetric parallelogramic</source>
        <translation>paralelogramo simétrico</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="527"/>
        <source>p3</source>
        <translation>p3</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="527"/>
        <source>hexagonal 3-rotative</source>
        <translation>hexagonal con 3 rotaciones</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="529"/>
        <source>p4</source>
        <translation>p4</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="529"/>
        <source>square 4-rotative</source>
        <translation>cuadrado con 4 rotaciones</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="531"/>
        <source>p6</source>
        <translation>p6</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="531"/>
        <source>hexagonal 6-rotative</source>
        <translation>hexagonal con 6 rotaciones</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="533"/>
        <source>pg</source>
        <translation>pg</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="533"/>
        <source>gliding rectangular</source>
        <translation>gliding rectangular</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="535"/>
        <source>pgg</source>
        <translation>pgg</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="535"/>
        <source>bi-gliding rectangular</source>
        <translation>bi-gliding rectangular</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="537"/>
        <source>pm</source>
        <translation>pm</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="537"/>
        <source>mono-symmetric rectangular</source>
        <translation>rectangular monosimétrico</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="539"/>
        <source>cm</source>
        <translation>cm</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="539"/>
        <source>mono-symmetric rhombic</source>
        <translation>mono-symmetric rhombic</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="541"/>
        <source>pmg</source>
        <translation>pmg</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="541"/>
        <source>symmetric gliding rectangular</source>
        <translation>symmetric gliding rectangular</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="543"/>
        <source>pmm</source>
        <translation>pmm</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="543"/>
        <source>bi-symmetric rectangular</source>
        <translation>bisimétrico rectangular</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="545"/>
        <source>cmm</source>
        <translation>cmm</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="545"/>
        <source>bi-symmetric rhombic</source>
        <translation>bisimétrico romboidal</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="547"/>
        <source>p4g</source>
        <translation>p4g</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="547"/>
        <source>gliding 4-rotative square</source>
        <translation>gliding 4-rotative square</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="549"/>
        <source>p3m1</source>
        <translation>p3m1</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="549"/>
        <source>tri-symmetric hexagonal</source>
        <translation>trisimétrico hexagonal</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="551"/>
        <source>p31m</source>
        <translation>p31m</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="551"/>
        <source>symmetric 3-rotative hexagonal</source>
        <translation>symmetric 3-rotative hexagonal</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="553"/>
        <source>p4m</source>
        <translation>p4m</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="553"/>
        <source>totally symmetric square</source>
        <translation>cuadrado totalmente simétrico</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="555"/>
        <source>p6m</source>
        <translation>p6m</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="555"/>
        <source>totally symmetric hexagonal</source>
        <translation>hexágono totalmente simétrico</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="560"/>
        <source>Type {0}: {1} [{2}]</source>
        <translation>Tipo {0} : {1} [{2}]</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="568"/>
        <source>A context menu is available by right-clicking</source>
        <translation>Hay un menú contextual disponible haciendo click derecho</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="456"/>
        <source>E&amp;dit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="463"/>
        <source>Expor&amp;t</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="276"/>
        <source>F1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="465"/>
        <source>Export as PNG File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="469"/>
        <source>Export as SVG File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="479"/>
        <source>Export Tile as PNG</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="483"/>
        <source>Export Tile as SVG</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="485"/>
        <source>Export the Tile as SVG File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="487"/>
        <source>Export Fundamental Set as PNG</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="489"/>
        <source>Export the Fundamental Set of Tiles as PNG File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="492"/>
        <source>Export Fundamental Set as SVG</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="494"/>
        <source>Export the Fundamental Set of Tiles as SVG File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="497"/>
        <source>Export Tiling as PNG</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="499"/>
        <source>Export the Tiling as PNG File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="501"/>
        <source>Export Tiling as SVG</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="508"/>
        <source>&amp;HelpPage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="510"/>
        <source>Show the Help in your browser</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="516"/>
        <source>&amp;Explanations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="518"/>
        <source>Displaying explanations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="460"/>
        <source>Show the Brush</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="447"/>
        <source>To create a .desktop file launcher in the folder of your choice</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="445"/>
        <source>Create a launcher</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/views.py" line="98"/>
        <source>The tile is crossed!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="607"/>
        <source>Choose the Directory where the desktop file will be created</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="193"/>
        <source>Cols:</source>
        <translation>Columnas:</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="199"/>
        <source>Rows:</source>
        <translation>Filas:</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="65"/>
        <source>Pen &amp;Width:</source>
        <translation>%Ancho de la pluma</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="70"/>
        <source>Solid</source>
        <translation>Sólido</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="73"/>
        <source>Dash</source>
        <translation>Raya</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="76"/>
        <source>Dot</source>
        <translation>Punto</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="79"/>
        <source>Dash Dot</source>
        <translation>Raya Punto</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="82"/>
        <source>Dash Dot Dot</source>
        <translation>Raya Punto Punto</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="85"/>
        <source>None</source>
        <translation>Ninguno</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="89"/>
        <source>&amp;Pen Style:</source>
        <translation>Estilo de la &amp;pluma</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="94"/>
        <source>Miter</source>
        <translation>Mitra</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="97"/>
        <source>Bevel</source>
        <translation>Bisel</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="100"/>
        <source>Round</source>
        <translation>Redondo</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="104"/>
        <source>Pen &amp;Join:</source>
        <translation>Unir &amp;pluma:</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="108"/>
        <source>&amp;Color</source>
        <translation>&amp;Color</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="110"/>
        <source>Pen Color:</source>
        <translation>Color de pluma:</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="277"/>
        <source>&amp;OK</source>
        <translation>&amp;OK</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="281"/>
        <source>C&amp;ancel</source>
        <translation>C&amp;ancelar</translation>
    </message>
    <message>
        <location filename="../libs/utils_about.py" line="116"/>
        <source>About {0}</source>
        <translation>Acerca de {0}</translation>
    </message>
    <message>
        <location filename="../libs/utils_about.py" line="76"/>
        <source>(version {0})</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="299"/>
        <source>&amp;Recent Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="408"/>
        <source>No name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="473"/>
        <source>TILE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="475"/>
        <source>SET</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="477"/>
        <source>TILING</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_exports.py" line="161"/>
        <source>Cannot open file {0}.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
