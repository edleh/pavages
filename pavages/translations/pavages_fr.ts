<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="2.0" language="fr_FR" sourcelanguage="">
<context>
    <name>main</name>
    <message>
        <location filename="../libs/utils_about.py" line="103"/>
        <source>&amp;Close</source>
        <translation>&amp;Fermer</translation>
    </message>
    <message>
        <location filename="../libs/utils_about.py" line="95"/>
        <source>About</source>
        <translation>À propos</translation>
    </message>
    <message>
        <location filename="../libs/utils_about.py" line="98"/>
        <source>License</source>
        <translation>Licence</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="514"/>
        <source>About this Application</source>
        <translation>À propos de ce programme</translation>
    </message>
    <message>
        <location filename="../libs/views.py" line="474"/>
        <source>A new Point will be create with the same position</source>
        <translation>Un nouveau point sera créé à la même position</translation>
    </message>
    <message>
        <location filename="../libs/views.py" line="479"/>
        <source>Delete this Point</source>
        <translation>Supprimer ce point</translation>
    </message>
    <message>
        <location filename="../libs/views.py" line="483"/>
        <source>Set the Line Style</source>
        <translation>Modifier le style des lignes du contour</translation>
    </message>
    <message>
        <location filename="../libs/views.py" line="485"/>
        <source>Select Brush</source>
        <translation>Sélectionner la brosse</translation>
    </message>
    <message>
        <location filename="../libs/views.py" line="487"/>
        <source>Select the Brush</source>
        <translation>Sélectionner la brosse</translation>
    </message>
    <message>
        <location filename="../libs/views.py" line="491"/>
        <source>Set the Tiling Config</source>
        <translation>Modifier la configuration du pavage</translation>
    </message>
    <message>
        <location filename="../libs/views.py" line="540"/>
        <source>Create a Point on arc {0}</source>
        <translation>Créer un point sur l&apos;arc {0}</translation>
    </message>
    <message>
        <location filename="../libs/views.py" line="545"/>
        <source>Create a new Point</source>
        <translation>Créer un point</translation>
    </message>
    <message>
        <location filename="../libs/views.py" line="550"/>
        <source>&amp;Colors</source>
        <translation>&amp;Couleurs</translation>
    </message>
    <message>
        <location filename="../libs/views.py" line="554"/>
        <source>Color Tile {0}</source>
        <translation>Couleur du pavé {0}</translation>
    </message>
    <message>
        <location filename="../libs/views.py" line="559"/>
        <source>Change the Tile Color</source>
        <translation>Changer la couleur du pavé</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="426"/>
        <source>&amp;File</source>
        <translation>&amp;Fichier</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="506"/>
        <source>&amp;Help</source>
        <translation>&amp;Aide</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="428"/>
        <source>&amp;Open</source>
        <translation>&amp;Ouvrir</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="432"/>
        <source>&amp;Save</source>
        <translation>&amp;Enregistrer</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="436"/>
        <source>Save &amp;as</source>
        <translation>Enregistrer &amp;sous</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="451"/>
        <source>&amp;Quit</source>
        <translation>&amp;Quitter</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="512"/>
        <source>&amp;About</source>
        <translation>À &amp;propos</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="423"/>
        <source>toolBarFile</source>
        <translation>Barre d&apos;outils fichiers</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="430"/>
        <source>Open a File</source>
        <translation>Ouvrir un fichier</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="434"/>
        <source>Save the File</source>
        <translation>Enregistrer le fichier</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="453"/>
        <source>Quit Application</source>
        <translation>Quitter le programme</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="458"/>
        <source>&amp;Brush</source>
        <translation>&amp;Brosse</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="503"/>
        <source>Export the Tiling as SVG File</source>
        <translation>Exporter le pavage comme fichier image SVG</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="481"/>
        <source>Export the Tile as PNG File</source>
        <translation>Exporter le pavé comme fichier image PNG</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="438"/>
        <source>Save the File with a different name</source>
        <translation>Enregistrer le fichier sous un autre nom</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="441"/>
        <source>&amp;Language</source>
        <translation>&amp;Langue</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="443"/>
        <source>Choose Language</source>
        <translation>Choisir la langue</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="90"/>
        <source>LanguageName</source>
        <translation>Français</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="829"/>
        <source>Open</source>
        <translation>Ouvrir</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="860"/>
        <source>Pavage Files (*.pav)</source>
        <translation>Fichiers de pavages (*.pav)</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="860"/>
        <source>Save as</source>
        <translation>Enregistrer sous</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="471"/>
        <source>SVG Files (*.svg)</source>
        <translation>Fichiers SVG (*.svg)</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="761"/>
        <source>PNG Files (*.png)</source>
        <translation>Fichiers PNG (*.png)</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="791"/>
        <source>File must be saved ?</source>
        <translation>Faut il enregistrer le fichier ?</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="793"/>
        <source>Save ?</source>
        <translation>Enregistrer ?</translation>
    </message>
    <message>
        <location filename="../libs/utils_exports.py" line="376"/>
        <source>Cannot write file {0}:
{1}.</source>
        <translation>Impossible d&apos;enregistrer le fichier {0} :
{1}.</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="523"/>
        <source>p1</source>
        <translation>p1</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="523"/>
        <source>asymmetric parallelogramic</source>
        <translation>parallélogrammique asymétrique</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="525"/>
        <source>p2</source>
        <translation>p2</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="525"/>
        <source>symmetric parallelogramic</source>
        <translation>parallélogrammique symétrique</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="527"/>
        <source>p3</source>
        <translation>p3</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="527"/>
        <source>hexagonal 3-rotative</source>
        <translation>héxagonal 3-rotatif</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="529"/>
        <source>p4</source>
        <translation>p4</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="529"/>
        <source>square 4-rotative</source>
        <translation>carré 4-rotatif</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="531"/>
        <source>p6</source>
        <translation>p6</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="531"/>
        <source>hexagonal 6-rotative</source>
        <translation>héxagonal 6-rotatif</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="533"/>
        <source>pg</source>
        <translation>pg</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="533"/>
        <source>gliding rectangular</source>
        <translation>rectangulaire glissant</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="535"/>
        <source>pgg</source>
        <translation>pgg</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="535"/>
        <source>bi-gliding rectangular</source>
        <translation>rectangulaire biglissant</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="537"/>
        <source>pm</source>
        <translation>pm</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="537"/>
        <source>mono-symmetric rectangular</source>
        <translation>rectangulaire monosymétrique</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="539"/>
        <source>cm</source>
        <translation>cm</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="539"/>
        <source>mono-symmetric rhombic</source>
        <translation>rhombique monosymétrique</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="541"/>
        <source>pmg</source>
        <translation>pmg</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="541"/>
        <source>symmetric gliding rectangular</source>
        <translation>rectangulaire glissant symétrique</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="543"/>
        <source>pmm</source>
        <translation>pmm</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="543"/>
        <source>bi-symmetric rectangular</source>
        <translation>rectangulaire bisymétrique</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="545"/>
        <source>cmm</source>
        <translation>cmm</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="545"/>
        <source>bi-symmetric rhombic</source>
        <translation>rhombique bisymétrique</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="547"/>
        <source>p4g</source>
        <translation>p4g</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="547"/>
        <source>gliding 4-rotative square</source>
        <translation>carré 4-rotatif glissant</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="549"/>
        <source>p3m1</source>
        <translation>p3m1</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="549"/>
        <source>tri-symmetric hexagonal</source>
        <translation>héxagonal trisymétrique</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="551"/>
        <source>p31m</source>
        <translation>p31m</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="551"/>
        <source>symmetric 3-rotative hexagonal</source>
        <translation>héxagonal 3-rotatif symétrique</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="553"/>
        <source>p4m</source>
        <translation>p4m</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="553"/>
        <source>totally symmetric square</source>
        <translation>carré totalement symétrique</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="555"/>
        <source>p6m</source>
        <translation>p6m</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="555"/>
        <source>totally symmetric hexagonal</source>
        <translation>héxagonal totalement symétrique</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="560"/>
        <source>Type {0}: {1} [{2}]</source>
        <translation>Type n°{0} : {1} [{2}]</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="568"/>
        <source>A context menu is available by right-clicking</source>
        <translation>Un menu contextuel est disponible par clic droit</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="456"/>
        <source>E&amp;dit</source>
        <translation>É&amp;dition</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="463"/>
        <source>Expor&amp;t</source>
        <translation>Expor&amp;ter</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="276"/>
        <source>F1</source>
        <translation>F1</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="465"/>
        <source>Export as PNG File</source>
        <translation>Exporter en fichier PNG</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="469"/>
        <source>Export as SVG File</source>
        <translation>Exporter en fichier SVG</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="479"/>
        <source>Export Tile as PNG</source>
        <translation>Exporter le pavé en PNG</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="483"/>
        <source>Export Tile as SVG</source>
        <translation>Exporter le pavé en SVG</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="485"/>
        <source>Export the Tile as SVG File</source>
        <translation>Exporter le pavé comme fichier image SVG</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="487"/>
        <source>Export Fundamental Set as PNG</source>
        <translation>Exporter l&apos;ensemble fondamental en PNG</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="489"/>
        <source>Export the Fundamental Set of Tiles as PNG File</source>
        <translation>Exporter l&apos;ensemble fondamental de pavés comme fichier image PNG</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="492"/>
        <source>Export Fundamental Set as SVG</source>
        <translation>Exporter l&apos;ensemble fondamental en SVG</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="494"/>
        <source>Export the Fundamental Set of Tiles as SVG File</source>
        <translation>Exporter l&apos;ensemble fondamental de pavés comme fichier image SVG</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="497"/>
        <source>Export Tiling as PNG</source>
        <translation>Exporter le pavage en PNG</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="499"/>
        <source>Export the Tiling as PNG File</source>
        <translation>Exporter le pavage comme fichier image PNG</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="501"/>
        <source>Export Tiling as SVG</source>
        <translation>Exporter le pavage en SVG</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="508"/>
        <source>&amp;HelpPage</source>
        <translation>&amp;Aide en ligne</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="510"/>
        <source>Show the Help in your browser</source>
        <translation>Afficher l&apos;aide dans votre navigateur</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="516"/>
        <source>&amp;Explanations</source>
        <translation>&amp;Explications</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="518"/>
        <source>Displaying explanations</source>
        <translation>Afficher les explications</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="460"/>
        <source>Show the Brush</source>
        <translation>Afficher la brosse</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="447"/>
        <source>To create a .desktop file launcher in the folder of your choice</source>
        <translation>Pour créer un lanceur (fichier *.desktop) dans le dossier de votre choix</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="607"/>
        <source>Choose the Directory where the desktop file will be created</source>
        <translation>Choisissez le dossier où le lanceur sera créé</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="445"/>
        <source>Create a launcher</source>
        <translation>Créer un lanceur</translation>
    </message>
    <message>
        <location filename="../libs/views.py" line="98"/>
        <source>The tile is crossed!</source>
        <translation>Le pavé est croisé !</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="193"/>
        <source>Cols:</source>
        <translation>Colonnes :</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="199"/>
        <source>Rows:</source>
        <translation>Lignes :</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="65"/>
        <source>Pen &amp;Width:</source>
        <translation>&amp;Largeur :</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="70"/>
        <source>Solid</source>
        <translation>Traits pleins</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="73"/>
        <source>Dash</source>
        <translation>Tirets</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="76"/>
        <source>Dot</source>
        <translation>Points</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="79"/>
        <source>Dash Dot</source>
        <translation>Tirets Points</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="82"/>
        <source>Dash Dot Dot</source>
        <translation>Tirets Points Points</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="85"/>
        <source>None</source>
        <translation>Aucun</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="89"/>
        <source>&amp;Pen Style:</source>
        <translation>&amp;Style :</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="94"/>
        <source>Miter</source>
        <translation>Droits</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="97"/>
        <source>Bevel</source>
        <translation>Biseautés</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="100"/>
        <source>Round</source>
        <translation>Arrondis</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="104"/>
        <source>Pen &amp;Join:</source>
        <translation>&amp;Raccords :</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="108"/>
        <source>&amp;Color</source>
        <translation>&amp;Couleur</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="110"/>
        <source>Pen Color:</source>
        <translation>Couleur :</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="277"/>
        <source>&amp;OK</source>
        <translation>&amp;OK</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="281"/>
        <source>C&amp;ancel</source>
        <translation>&amp;Annuler</translation>
    </message>
    <message>
        <location filename="../libs/views.py" line="472"/>
        <source>Duplicate this Point</source>
        <translation>Dupliquer ce point</translation>
    </message>
    <message>
        <location filename="../libs/views.py" line="481"/>
        <source>Line Style</source>
        <translation>Style des lignes</translation>
    </message>
    <message>
        <location filename="../libs/views.py" line="489"/>
        <source>Tiling Config</source>
        <translation>Configuration du pavage</translation>
    </message>
    <message>
        <location filename="../libs/utils_about.py" line="116"/>
        <source>About {0}</source>
        <translation>À propos de {0}</translation>
    </message>
    <message>
        <location filename="../libs/utils_about.py" line="76"/>
        <source>(version {0})</source>
        <translation>(version {0})</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="299"/>
        <source>&amp;Recent Files</source>
        <translation>Fichiers &amp;récents</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="408"/>
        <source>No name</source>
        <translation>Sans nom</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="473"/>
        <source>TILE</source>
        <translation>PAVÉ</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="475"/>
        <source>SET</source>
        <translation>EF</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="477"/>
        <source>TILING</source>
        <translation>PAVAGE</translation>
    </message>
    <message>
        <location filename="../libs/utils_exports.py" line="161"/>
        <source>Cannot open file {0}.</source>
        <translation>Impossible d&apos;ouvrir le fichier {0}.</translation>
    </message>
</context>
</TS>
