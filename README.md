# PAVAGES

* **Website:** http://pascal.peter.free.fr/pavages.html
* **Email:** pascal.peter at free.fr
* **License:** GNU General Public License (version 3)
* **Copyright:** (c) 1998-2016

----

#### The tools used to develop Pavages
* [Python](https://www.python.org): programming language
* [Qt](https://www.qt.io): "toolkit" comprehensive (graphical user interface and a lot of things)
* [PyQt](https://riverbankcomputing.com): link between Python and Qt

#### Other libraries used and other stuff:
* [marked](https://github.com/chjj/marked): to view the Markdown files

#### initial translation into English
* Olivier Aumaire (Olivieraumaire at aol.com)
* Florence Aumaire-Grevet

#### Miscellaneous
* [GNU GPL 3](http://www.gnu.org/copyleft/gpl.html): GNU General Public License
